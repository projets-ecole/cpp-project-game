#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

#include "GameEngine.h"


class cPhysicsObject
{
public:
	cPhysicsObject(float x = 0.0f, float y = 0.0f)
	{
		px = x;
		py = y;
	}

public:
	float px = 0.0f;				// Position
	float py = 0.0f;
	float vx = 0.0f;				// V�locit�
	float vy = 0.0f;
	float ax = 0.0f;				// Acc�l�ration
	float ay = 0.0f;
	float radius = 4.0f;			// Lier les rectangles pour les collisions
	float fFriction = 0.0f;
	int nBounceBeforeDeath = -1;	// Combien de fois un objet peut rebondir avant de mourir
	bool bDead;						// Le bool�en qui indique � l'objet qu'il doit �tre supprim�
	bool bStable = false;			// Quand l'objet n'est plus en mouvement

	// La classe "abstract"
	virtual void Draw(GameEngine* engine, float fOffsetX, float fOffsetY, bool bPixel = false) = 0;
	virtual int BounceDeathAction() = 0;
	virtual bool Damage(float d) = 0;
};

class cDebris : public cPhysicsObject // Un petit d�bris qui rebondis
{
public:
	cDebris(float x = 0.0f, float y = 0.0f) : cPhysicsObject(x, y)
	{
		// Fixer � la v�locit� une direction et une taille al�atoire pour l'effet "boom" (boom est une fonction void appel� plus tard dans le code)
		vx = 10.0f * cosf(((float)rand() / (float)RAND_MAX) * 2.0f * 3.14159f);
		vy = 10.0f * sinf(((float)rand() / (float)RAND_MAX) * 2.0f * 3.14159f);
		radius = 1.0f;
		fFriction = 0.8f;
		bDead = false;
		bStable = false;
		nBounceBeforeDeath = 2; // Se d�barasser des d�bris apr�s deux rebonds
	}

	virtual void Draw(GameEngine* engine, float fOffsetX, float fOffsetY, bool bPixel = false)
	{
		engine->DrawWireFrameModel(vecModel, px - fOffsetX, py - fOffsetY, atan2f(vy, vx), bPixel ? 0.5f : radius, FG_DARK_GREEN);
	}

	virtual int BounceDeathAction()
	{
		return 0; // Rien, juste on supprime.
	}

	virtual bool Damage(float d)
	{
		return true; // Ne peux pas subir de d�g�ts
	}

private:
	static vector<pair<float, float>> vecModel;

};

vector<pair<float, float>> DefineDebris()
{
	// Un petit rectangle
	vector<pair<float, float>> vecModel;
	vecModel.push_back({ 0.0f, 0.0f });
	vecModel.push_back({ 1.0f, 0.0f });
	vecModel.push_back({ 1.0f, 1.0f });
	vecModel.push_back({ 0.0f, 1.0f });
	return vecModel;
}

vector<pair<float, float>> cDebris::vecModel = DefineDebris();

class cMissile : public cPhysicsObject // Comme son nom l'indique, c'est un missile (arme)
{
public:
	cMissile(float x = 0.0f, float y = 0.0f, float _vx = 0.0f, float _vy = 0.0f) : cPhysicsObject(x, y)
	{
		radius = 2.5f;
		fFriction = 0.5f;
		vx = _vx;
		vy = _vy;
		bDead = false;
		nBounceBeforeDeath = 1;
		bStable = false;
	}

	virtual void Draw(GameEngine* engine, float fOffsetX, float fOffsetY, bool bPixel = false)
	{
		engine->DrawWireFrameModel(vecModel, px - fOffsetX, py - fOffsetY, atan2f(vy, vx), bPixel ? 0.5f : radius, FG_BLACK);
	}

	virtual int BounceDeathAction()
	{
		return 20; // Permet de faire une grosse explosion (20) si on change cette valeur pour en mettre une plus grande alors l'explosion serra encore plus grosse et l'inverse fonctionne �galement.
	}

	virtual bool Damage(float d)
	{
		return true;
	}

private:
	static vector<pair<float, float>> vecModel;
};

vector<pair<float, float>> DefineMissile()
{
	// Le sprite du missile
	vector<pair<float, float>> vecModel;
	vecModel.push_back({ 0.0f, 0.0f });
	vecModel.push_back({ 1.0f, 1.0f });
	vecModel.push_back({ 2.0f, 1.0f });
	vecModel.push_back({ 2.5f, 0.0f });
	vecModel.push_back({ 2.0f, -1.0f });
	vecModel.push_back({ 1.0f, -1.0f });
	vecModel.push_back({ 0.0f, 0.0f });
	vecModel.push_back({ -1.0f, -1.0f });
	vecModel.push_back({ -2.5f, -1.0f });
	vecModel.push_back({ -2.0f, 0.0f });
	vecModel.push_back({ -2.5f, 1.0f });
	vecModel.push_back({ -1.0f, 1.0f });

	// R�gler la taille du missile � la taille de l'invit� de commande
	for (auto& v : vecModel)
	{
		v.first /= 1.5f; v.second /= 1.5f;
	}
	return vecModel;
}

vector<pair<float, float>> cMissile::vecModel = DefineMissile();

class cUnit : public cPhysicsObject // Cette classe d�fini le sprite des unit�s
{
public:
	cUnit(float x = 0.0f, float y = 0.0f) : cPhysicsObject(x, y)
	{
		radius = 3.5f;
		fFriction = 0.2f;
		bDead = false;
		nBounceBeforeDeath = -1;
		bStable = false;

		// Charger le sprite des unit�s
		if (sprUnit == nullptr)
			sprUnit = new Sprite(L"units.spr");
	}

	virtual void Draw(GameEngine* engine, float fOffsetX, float fOffsetY, bool bPixel = false)
	{
		if (bIsPlayable) // Dessiner le sprite des unit�s en fonction de leur couleur ainsi que la barre de points de vie
		{
			engine->DrawPartialSprite(px - fOffsetX - radius, py - fOffsetY - radius, sprUnit, nTeam * 8, 0, 8, 8);

			// Dessiner la bar de point de vie des unit�s
			for (int i = 0; i < 11 * fHealth; i++)
			{
				engine->Draw(px - 5 + i - fOffsetX, py + 5 - fOffsetY, PIXEL_SOLID, FG_BLUE);
				engine->Draw(px - 5 + i - fOffsetX, py + 6 - fOffsetY, PIXEL_SOLID, FG_BLUE);
			}
		}
		else // Dessiner la tombe que lache les unit�s � leur mort de la m�me couleur que l'�quipe (�quipe bleu donc tombe avec un trait bleu)
		{
			engine->DrawPartialSprite(px - fOffsetX - radius, py - fOffsetY - radius, sprUnit, nTeam * 8, 8, 8, 8);
		}
	}

	virtual int BounceDeathAction()
	{
		return 0; // Rien de nouveau vu que l'on return 0
	}

	virtual bool Damage(float d) // Retire des points de vie aux unit�s en fonction des d�g�ts subits
	{
		fHealth -= d;
		if (fHealth <= 0)
		{ // L'unit� est morte donc de ce fait, elle n'est plus jouable
			fHealth = 0.0f;
			bIsPlayable = false;
		}
		return fHealth > 0;
	}

public:
	float fShootAngle = 0.0f;
	float fHealth = 1.0f;
	int nTeam = 0;	// L'ID de l'�quipe � laquelle appartient l'unit�
	bool bIsPlayable = true;

private:
	static Sprite* sprUnit;
};

Sprite* cUnit::sprUnit = nullptr;



class cTeam // Classe qui cr�e une �quipe dans laquelle 4 unit�s seront cr��es 
{
public:
	vector<cUnit*> vecMembers;
	int nCurrentMember = 0;		// L'index au sein du vecteur (vector<cUnit*> vecMembers;) pour d�finir le tour de l'unit� (quand est-ce que l'unit� joue)
	int nTeamSize = 0;			// Nombre total d'unit� dans une �quipe d�fini � z�ro (Il y a 4 unit�s par �quipe)

	bool IsTeamAlive()
	{
		// On it�re sur tout les membres de l'�quipe, si une �quipe � encore des points de vie alors on return true
		bool bAllDead = false;
		for (auto w : vecMembers)
			bAllDead |= (w->fHealth > 0.0f);
		return bAllDead;
	}

	cUnit* GetNextMember()
	{
		// Retourne un pointeur au prochain membre de l'�quipe qui a la possiblit� de jouer
		do {
			nCurrentMember++;
			if (nCurrentMember >= nTeamSize) nCurrentMember = 0;
		} while (vecMembers[nCurrentMember]->fHealth <= 0);
		return vecMembers[nCurrentMember];
	}
};


// La classe principale du moteur du jeu
class Game : public GameEngine // Le jeu
{
public:
	Game()
	{
		m_sAppName = L"Clash of Hills";
	}

private:
	// La taille du terrain
	int nMapWidth = 1024;
	int nMapHeight = 512;
	char* map = nullptr;

	// Coordonner les cam�ras sur les axes x et y
	float fCameraPosX = 0.0f;
	float fCameraPosY = 0.0f;
	float fCameraPosXTarget = 0.0f;
	float fCameraPosYTarget = 0.0f;

	// Une liste des choses existant dans le jeu
	list<unique_ptr<cPhysicsObject>> listObjects;

	cPhysicsObject* pObjectUnderControl = nullptr;		// Le pointeur sur l'objet actuellement sous contr�le
	cPhysicsObject* pCameraTrackingObject = nullptr;	// Le pointeur sur l'objet que la cam�ra doit suivre

	// Flags that govern/are set by game state machine
	// Les drapeaux qui sont fix� par l'�t�t du jeu
	bool bZoomOut = false;					// Affiche toute la carte
	bool bGameIsStable = false;				// Tout les objets physique sont stable de base
	bool bEnablePlayerControl = true;		// Le joueur avec cette �t�t peut jouer, donc on redonne le contr�le des touches au joueur
	bool bEnableComputerControl = false;	// L'ordinateur reprend le contr�le
	bool bEnergising = false;				// L'arme est entrain de charger son tir
	bool bFireWeapon = false;				// L'arme doit �tre d�charg�
	bool bShowCountDown = false;			// Afficher le temps qui reste avant la fin du tour sur l'�cran
	bool bPlayerHasFired = false;			// Dans cette �t�t, l'arme est d�charg�

	float fEnergyLevel = 0.0f;				// L'�nergie accumul� en chargeant l'arme (seulement le joueur)
	float fTurnTime = 0.0f;					// Le temps restant du tour

	// Le vecteur qui stocke les �quipes
	vector<cTeam> vecTeams;

	// L'�quipe qui a actuellement le contr�le
	int nCurrentTeam = 0;

	// Les drapeaux de l'ordinateur (Intelligence Artificiel)
	bool bAI_Jump = false;				// L'IA appuie sur la touche pour sauter
	bool bAI_AimLeft = false;			// L'IA appuie sur la touche pour viser vers la gauche
	bool bAI_AimRight = false;			// L'IA appuie sur la touche pour viser vers la droite
	bool bAI_Energise = false;			// L'IA appuie sur la touche pour tirer


	float fAITargetAngle = 0.0f;		// L'angle vers lequel l'IA devra viser
	float fAITargetEnergy = 0.0f;		// La puissance que l'IA doit mettre dans son tir
	float fAISafePosition = 0.0f;		// Les coordonn�es vers lesquelles l'IA "consid�re" que c'est s�curis� d'aller
	cUnit* pAITargetUnit = nullptr;		// Le pointeur de l'IA qui c'est fait prendre comme cible 
	float fAITargetX = 0.0f;			// Les coordonn�es de la cible du missile
	float fAITargetY = 0.0f;

	enum GAME_STATE
	{
		GS_RESET = 0,
		GS_GENERATE_TERRAIN = 1,
		GS_GENERATING_TERRAIN,
		GS_ALLOCATE_UNITS,
		GS_ALLOCATING_UNITS,
		GS_START_PLAY,
		GS_CAMERA_MODE,
		GS_GAME_OVER1,
		GS_GAME_OVER2
	} nGameState, nNextState;


	enum AI_STATE
	{
		AI_ASSESS_ENVIRONMENT = 0,
		AI_MOVE,
		AI_CHOOSE_TARGET,
		AI_POSITION_FOR_TARGET,
		AI_AIM,
		AI_FIRE,
	} nAIState, nAINextState;

	virtual bool OnUserCreate()
	{
		// Cr�er la carte
		map = new  char[nMapWidth * nMapHeight];
		memset(map, 0, nMapWidth * nMapHeight * sizeof(char));

		// Fixe les �t�ts de base pour les IA
		nGameState = GS_RESET;
		nNextState = GS_RESET;
		nAIState = AI_ASSESS_ENVIRONMENT;
		nAINextState = AI_ASSESS_ENVIRONMENT;

		bGameIsStable = false;
		return true;
	}

	virtual bool OnUserUpdate(float fElapsedTime)
	{
		// La touche tab permet de switcher entre une vue globale de la carte et une vue centrer sur l'unit�
		if (m_keys[VK_TAB].bReleased)
			bZoomOut = !bZoomOut;

		// Mouse Edge Map Scroll
		// Permet de d�placer la cam�ra pour se d�placer sur la carte quand la souris atteint le bord d'un des axes de la console
		float fMapScrollSpeed = 400.0f;
		if (m_mousePosX < 5) fCameraPosX -= fMapScrollSpeed * fElapsedTime;
		if (m_mousePosX > ScreenWidth() - 5) fCameraPosX += fMapScrollSpeed * fElapsedTime;
		if (m_mousePosY < 5) fCameraPosY -= fMapScrollSpeed * fElapsedTime;
		if (m_mousePosY > ScreenHeight() - 5) fCameraPosY += fMapScrollSpeed * fElapsedTime;

		// Le superviseur 
		switch (nGameState)
		{
		case GS_RESET:
		{
			bEnablePlayerControl = false;
			bGameIsStable = false;
			bPlayerHasFired = false;
			bShowCountDown = false;
			nNextState = GS_GENERATE_TERRAIN;
		}
		break;

		case GS_GENERATE_TERRAIN:
		{
			bZoomOut = true;
			CreateMap();
			bGameIsStable = false;
			bShowCountDown = false;
			nNextState = GS_GENERATING_TERRAIN;
		}
		break;

		case GS_GENERATING_TERRAIN:
		{
			bShowCountDown = false;
			if (bGameIsStable)
				nNextState = GS_ALLOCATE_UNITS;
		}
		break;

		case GS_ALLOCATE_UNITS:
		{
			// D�ploie les �quipes sur la carte
			int nTeams = 4;
			int nUnitsPerTeam = 4;

			// Calculate spacing of units and teams
			// Calculer l'espacement entre les diff�rentes unit�s de chaque �quipes (pour �viter que les unit�s apparaissent les unes sur les autres)
			float fSpacePerTeam = (float)nMapWidth / (float)nTeams;
			float fSpacePerUnit = fSpacePerTeam / (nUnitsPerTeam * 2.0f);

			// Cr�er les �quipes
			for (int t = 0; t < nTeams; t++)
			{
				vecTeams.emplace_back(cTeam());
				float fTeamMiddle = (fSpacePerTeam / 2.0f) + (t * fSpacePerTeam);
				for (int w = 0; w < nUnitsPerTeam; w++)
				{
					float fUnitX = fTeamMiddle - ((fSpacePerUnit * (float)nUnitsPerTeam) / 2.0f) + w * fSpacePerUnit;
					float fUnitY = 0.0f;

					// Ajouter les unit�s dans les �quipes
					cUnit* unit = new cUnit(fUnitX, fUnitY);
					unit->nTeam = t;
					listObjects.push_back(unique_ptr<cUnit>(unit));
					vecTeams[t].vecMembers.push_back(unit);
					vecTeams[t].nTeamSize = nUnitsPerTeam;
				}

				vecTeams[t].nCurrentMember = 0;
			}

			// Selectionner la premi�re unit� que va contr�ler le joueur pour donner les contr�les et que la cam�ra suive cette unit� pr�cise
			pObjectUnderControl = vecTeams[0].vecMembers[vecTeams[0].nCurrentMember];
			pCameraTrackingObject = pObjectUnderControl;
			bShowCountDown = false;
			nNextState = GS_ALLOCATING_UNITS;
		}
		break;

		case GS_ALLOCATING_UNITS: // Au lancement du jeu, Attendre que les unit�s tombent au sol avant d�marrer le jeu
		{
			if (bGameIsStable)
			{
				bEnablePlayerControl = true;
				bEnableComputerControl = false;
				fTurnTime = 15.0f;
				bZoomOut = false;
				nNextState = GS_START_PLAY;
			}
		}
		break;

		case GS_START_PLAY:
		{
			bShowCountDown = true;

			// Si le joueur a d�charg� son arme ou que le temps tourne toujours alors on change d'�t�t
			if (bPlayerHasFired || fTurnTime <= 0.0f)
				nNextState = GS_CAMERA_MODE;
		}
		break;

		case GS_CAMERA_MODE: //La cam�ra suit le centre d'inter�t (dans notre cas l'objet) jusqu'a ce que le moteur de jeu soit op�rationnel
		{
			bEnableComputerControl = false;
			bEnablePlayerControl = false;
			bPlayerHasFired = false;
			bShowCountDown = false;
			fEnergyLevel = 0.0f;

			if (bGameIsStable) // Une fois op�rationnel, choisir l'unit�
			{
				// Get Next Team, if there is no next team, game is over
				// Passer � l'�quipe suivante, si il n'y a plus de "prochaine �quipe" alors le jeu est termin� 
				int nOldTeam = nCurrentTeam;
				do {
					nCurrentTeam++;
					nCurrentTeam %= vecTeams.size();
				} while (!vecTeams[nCurrentTeam].IsTeamAlive());

				// Ne plus donner le contr�le au joueur si c'est au tour de l'IA de jouer
				if (nCurrentTeam == 0) // L'�quipe du joueur
				{
					bEnablePlayerControl = true;	// Interchanger tour � tour pour que toute les �quipes des IA jouent
					bEnableComputerControl = false;
				}
				else // L'�quipe de l'IA
				{
					bEnablePlayerControl = false;
					bEnableComputerControl = true;
				}

				// "Configurer" les contr�les et la cam�ra
				pObjectUnderControl = vecTeams[nCurrentTeam].GetNextMember();
				pCameraTrackingObject = pObjectUnderControl;
				fTurnTime = 15.0f;
				bZoomOut = false;
				nNextState = GS_START_PLAY;

				// Si aucune �quipes diff�rentes n'a pu �tre trouv�
				if (nCurrentTeam == nOldTeam)
				{
					// Le jeu est termin�. L'�quipe actuelle a gagn� !!!!!
					nNextState = GS_GAME_OVER1;
				}
			}
		}
		break;

		case GS_GAME_OVER1: // Quand le jeu est termin�, on d�zoome pour avoir une vue d'ensemble de la carte et on balance plein de missiles pour d�truire une partie de la carte
		{
			bEnableComputerControl = false;
			bEnablePlayerControl = false;
			bZoomOut = true;
			bShowCountDown = false;

			for (int i = 0; i < 100; i++)
			{
				int nBombX = rand() % nMapWidth;
				int nBombY = rand() % (nMapHeight / 2);
				listObjects.push_back(unique_ptr<cMissile>(new cMissile(nBombX, nBombY, 0.0f, 0.5f)));
			}

			nNextState = GS_GAME_OVER2;
		}
		break;

		case GS_GAME_OVER2: // La cam�ra reste sur place et on regarde la carte ce d�truire :D
		{
			bEnableComputerControl = false;
			bEnablePlayerControl = false;
			// Il n'y a pas d'�chappatoire � cette �t�t
		}
		break;

		}

		// Les "�t�ts" de l'IA. L'IA poss�de 3 �t�ts : 
		// - Jouer d�fensivement
		// - Jouer aggressivement
		// - Jouer de mani�re passive
		if (bEnableComputerControl)
		{
			switch (nAIState)
			{
			case AI_ASSESS_ENVIRONMENT:
			{

				int nAction = rand() % 3;
				if (nAction == 0) // Jouer d�fensivement - Se d�placer loin de son �quipe pour �tre plus en s�curit�
				{
					// On trouve l'alli� le plus proche et on s'en �carte
					float fNearestAllyDistance = INFINITY; float fDirection = 0;
					cUnit* origin = (cUnit*)pObjectUnderControl;

					for (auto w : vecTeams[nCurrentTeam].vecMembers)
					{
						if (w != pObjectUnderControl)
						{
							if (fabs(w->px - origin->px) < fNearestAllyDistance)
							{
								fNearestAllyDistance = fabs(w->px - origin->px);
								fDirection = (w->px - origin->px) < 0.0f ? 1.0f : -1.0f;
							}
						}
					}

					if (fNearestAllyDistance < 50.0f)
						fAISafePosition = origin->px + fDirection * 80.0f;
					else
						fAISafePosition = origin->px;
				}

				if (nAction == 1) // Jouer aggressivement - S'avancer vers le centre de la carte
				{
					cUnit* origin = (cUnit*)pObjectUnderControl;
					float fDirection = ((float)(nMapWidth / 2.0f) - origin->px) < 0.0f ? -1.0f : 1.0f;
					fAISafePosition = origin->px + fDirection * 200.0f;
				}

				if (nAction == 2) // Jouer de mani�re passive - Ne pas bouger
				{
					cUnit* origin = (cUnit*)pObjectUnderControl;
					fAISafePosition = origin->px;
				}

				// Faire en sorte que l'IA ne sorte pas de la carte (on impose des limites)
				if (fAISafePosition <= 20.0f) fAISafePosition = 20.0f;
				if (fAISafePosition >= nMapWidth - 20.0f) fAISafePosition = nMapWidth - 20.0f;
				nAINextState = AI_MOVE;
			}
			break;

			case AI_MOVE:
			{
				cUnit* origin = (cUnit*)pObjectUnderControl;
				if (fTurnTime >= 8.0f && origin->px != fAISafePosition)
				{
					// S'avancer vers la cible jusqu'� ce qu'elle soit � porter					
					if (fAISafePosition < origin->px && bGameIsStable)
					{
						origin->fShootAngle = -3.14159f * 0.6f;
						bAI_Jump = true;
						nAINextState = AI_MOVE;
					}

					if (fAISafePosition > origin->px && bGameIsStable)
					{
						origin->fShootAngle = -3.14159f * 0.4f;
						bAI_Jump = true;
						nAINextState = AI_MOVE;
					}
				}
				else
					nAINextState = AI_CHOOSE_TARGET;
			}
			break;

			case AI_CHOOSE_TARGET: // L'unit� a finit de bouger, maintenant l'IA doit choisir sa cible
			{
				bAI_Jump = false;

				// S�lectionne une cible d'une �quipe adverse (et donc pas de sa propre �quipe)
				cUnit* origin = (cUnit*)pObjectUnderControl;
				int nCurrentTeam = origin->nTeam;
				int nTargetTeam = 0;
				do {
					nTargetTeam = rand() % vecTeams.size();
				} while (nTargetTeam == nCurrentTeam || !vecTeams[nTargetTeam].IsTeamAlive());

				//La strat�gie agressive consiste � viser l'ennemis avec le plus de point de vie
				cUnit* mostHealthyUnit = vecTeams[nTargetTeam].vecMembers[0];
				for (auto w : vecTeams[nTargetTeam].vecMembers)
					if (w->fHealth > mostHealthyUnit->fHealth)
						mostHealthyUnit = w;

				pAITargetUnit = mostHealthyUnit;
				fAITargetX = mostHealthyUnit->px;
				fAITargetY = mostHealthyUnit->py;
				nAINextState = AI_POSITION_FOR_TARGET;
			}
			break;

			case AI_POSITION_FOR_TARGET: // Calcule la trajectoire de la cible de l'IA, si jamais elle a besoin d'avancer, elle le fera
			{
				cUnit* origin = (cUnit*)pObjectUnderControl;
				float dy = -(fAITargetY - origin->py);
				float dx = -(fAITargetX - origin->px);
				float fSpeed = 30.0f;
				float fGravity = 2.0f;

				bAI_Jump = false;

				float a = fSpeed * fSpeed * fSpeed * fSpeed - fGravity * (fGravity * dx * dx + 2.0f * dy * fSpeed * fSpeed);

				if (a < 0) // Lorsque la cible est au-del� de la port�
				{
					if (fTurnTime >= 5.0f)
					{
						// Avance vers la cible jusqu'� arriver � port� de feu
						if (pAITargetUnit->px < origin->px && bGameIsStable)
						{
							origin->fShootAngle = -3.14159f * 0.6f;
							bAI_Jump = true;
							nAINextState = AI_POSITION_FOR_TARGET;
						}

						if (pAITargetUnit->px > origin->px && bGameIsStable)
						{
							origin->fShootAngle = -3.14159f * 0.4f;
							bAI_Jump = true;
							nAINextState = AI_POSITION_FOR_TARGET;
						}
					}
					else
					{
						// L'IA est bloqu� et dans ce cas elle tire juste vers l'ennemis (le plus souvent dans la paroie qui la bloque et donc dans sa face :D)
						fAITargetAngle = origin->fShootAngle;
						fAITargetEnergy = 0.75f;
						nAINextState = AI_AIM;
					}
				}
				else
				{
					//Lorsque l'IA est assez proche il peut commencer � viser
					float b1 = fSpeed * fSpeed + sqrtf(a);
					float b2 = fSpeed * fSpeed - sqrtf(a);

					float fTheta1 = atanf(b1 / (fGravity * dx)); // Hauteur maximale
					float fTheta2 = atanf(b2 / (fGravity * dx)); // Hauteur minimale

					// We'll use max as its a greater chance of avoiding obstacles
					fAITargetAngle = fTheta1 - (dx > 0 ? 3.14159f : 0.0f);
					float fFireX = cosf(fAITargetAngle);
					float fFireY = sinf(fAITargetAngle);

					// L'ia et max� au 3/4 de sa puissance de tire
					fAITargetEnergy = 0.75f;
					nAINextState = AI_AIM;
				}
			}
			break;

			case AI_AIM: // La phase o� l'IA vise avec son curseur 
			{
				cUnit* unit = (cUnit*)pObjectUnderControl;

				bAI_AimLeft = false;
				bAI_AimRight = false;
				bAI_Jump = false;

				if (unit->fShootAngle < fAITargetAngle)
					bAI_AimRight = true;
				else
					bAI_AimLeft = true;

				// Une fois les curseurs align�s, l'IA fait feu
				if (fabs(unit->fShootAngle - fAITargetAngle) <= 0.001f)
				{
					bAI_AimLeft = false;
					bAI_AimRight = false;
					fEnergyLevel = 0.0f;
					nAINextState = AI_FIRE;
				}
				else
					nAINextState = AI_AIM;
			}
			break;

			case AI_FIRE:
			{
				bAI_Energise = true;
				bFireWeapon = false;
				bEnergising = true;

				if (fEnergyLevel >= fAITargetEnergy)
				{
					bFireWeapon = true;
					bAI_Energise = false;
					bEnergising = false;
					bEnableComputerControl = false;
					nAINextState = AI_ASSESS_ENVIRONMENT;
				}
			}
			break;

			}
		}

		// Diminue le temps de rotation
		fTurnTime -= fElapsedTime;

		if (pObjectUnderControl != nullptr)
		{
			pObjectUnderControl->ax = 0.0f;

			if (pObjectUnderControl->bStable)
			{
				if ((bEnablePlayerControl && m_keys[L'Z'].bPressed) || (bEnableComputerControl && bAI_Jump))
				{
					float a = ((cUnit*)pObjectUnderControl)->fShootAngle;

					pObjectUnderControl->vx = 4.0f * cosf(a);
					pObjectUnderControl->vy = 8.0f * sinf(a);
					pObjectUnderControl->bStable = false;

					bAI_Jump = false;
				}

				if ((bEnablePlayerControl && m_keys[L'S'].bHeld) || (bEnableComputerControl && bAI_AimRight))
				{
					cUnit* unit = (cUnit*)pObjectUnderControl;
					unit->fShootAngle += 1.0f * fElapsedTime;
					if (unit->fShootAngle > 3.14159f) unit->fShootAngle -= 3.14159f * 2.0f;
				}

				if ((bEnablePlayerControl && m_keys[L'A'].bHeld) || (bEnableComputerControl && bAI_AimLeft))
				{
					cUnit* unit = (cUnit*)pObjectUnderControl;
					unit->fShootAngle -= 1.0f * fElapsedTime;
					if (unit->fShootAngle < -3.14159f) unit->fShootAngle += 3.14159f * 2.0f;
				}

				if ((bEnablePlayerControl && m_keys[VK_SPACE].bPressed))
				{
					bFireWeapon = false;
					bEnergising = true;
					fEnergyLevel = 0.0f;
				}

				if ((bEnablePlayerControl && m_keys[VK_SPACE].bHeld) || (bEnableComputerControl && bAI_Energise))
				{
					if (bEnergising)
					{
						fEnergyLevel += 0.75f * fElapsedTime;
						if (fEnergyLevel >= 1.0f)
						{
							fEnergyLevel = 1.0f;
							bFireWeapon = true;
						}
					}
				}

				if ((bEnablePlayerControl && m_keys[VK_SPACE].bReleased))
				{
					if (bEnergising)
					{
						bFireWeapon = true;
					}

					bEnergising = false;
				}
			}

			if (pCameraTrackingObject != nullptr)
			{
				fCameraPosXTarget = pCameraTrackingObject->px - ScreenWidth() / 2;
				fCameraPosYTarget = pCameraTrackingObject->py - ScreenHeight() / 2;
				fCameraPosX += (fCameraPosXTarget - fCameraPosX) * 15.0f * fElapsedTime;
				fCameraPosY += (fCameraPosYTarget - fCameraPosY) * 15.0f * fElapsedTime;
			}

			if (bFireWeapon)
			{
				cUnit* unit = (cUnit*)pObjectUnderControl;

				// R�cup�re la position du personnage pour y g�n�rer le tire � partir de
				float ox = unit->px;
				float oy = unit->py;

				// R�cup�re la direction du tire
				float dx = cosf(unit->fShootAngle);
				float dy = sinf(unit->fShootAngle);

				// G�n�re le missile 
				cMissile* m = new cMissile(ox, oy, dx * 40.0f * fEnergyLevel, dy * 40.0f * fEnergyLevel);
				pCameraTrackingObject = m;
				listObjects.push_back(unique_ptr<cMissile>(m));

				// R�initialise la bare de chargement du tire une fois le projectile tir�
				bFireWeapon = false;
				fEnergyLevel = 0.0f;
				bEnergising = false;
				bPlayerHasFired = true;

				if (rand() % 100 >= 50)
					bZoomOut = true;
			}
		}

		// V�rifit les limites de la map
		if (fCameraPosX < 0) fCameraPosX = 0;
		if (fCameraPosX >= nMapWidth - ScreenWidth()) fCameraPosX = nMapWidth - ScreenWidth();
		if (fCameraPosY < 0) fCameraPosY = 0;
		if (fCameraPosY >= nMapHeight - ScreenHeight()) fCameraPosY = nMapHeight - ScreenHeight();

		// Boucle qui effectue 10 it�rations � chaque frame
		for (int z = 0; z < 10; z++)
		{
			// UMet � jour la physique de tout les objets
			for (auto& p : listObjects)
			{
				// Impl�mente la gravit�
				p->ay += 2.0f;

				// Met � jour la v�locit�
				p->vx += p->ax * fElapsedTime;
				p->vy += p->ay * fElapsedTime;

				// Met � jour les positions
				float fPotentialX = p->px + p->vx * fElapsedTime;
				float fPotentialY = p->py + p->vy * fElapsedTime;

				// Rement l'acc�l�ration � zero
				p->ax = 0.0f;
				p->ay = 0.0f;

				p->bStable = false;

				// Ici on V�rifit si il y a une collision avec le sol
				float fAngle = atan2f(p->vy, p->vx);
				float fResponseX = 0;
				float fResponseY = 0;
				bool bCollision = false;
				for (float r = fAngle - 3.14159f / 2.0f; r < fAngle + 3.14159f / 2.0f; r += 3.14159f / 4.0f)
				{
					// It�re � travers le demi-cercle les objets pr�sent dans le sens de d�placement de l'objet
					float fTestPosX = (p->radius) * cosf(r) + fPotentialX;
					float fTestPosY = (p->radius) * sinf(r) + fPotentialY;

					if (fTestPosX >= nMapWidth) fTestPosX = nMapWidth - 1;
					if (fTestPosY >= nMapHeight) fTestPosY = nMapHeight - 1;
					if (fTestPosX < 0) fTestPosX = 0;
					if (fTestPosY < 0) fTestPosY = 0;

					// Regarde si il y a du terrain dans l'angle du demi-cercle
					if (map[(int)fTestPosY * nMapWidth + (int)fTestPosX] > 0)
					{
						// Accumuler les points de collision pour donner un vecteur de r�ponse d'�chappement
						fResponseX += fPotentialX - fTestPosX;
						fResponseY += fPotentialY - fTestPosY;
						bCollision = true;
					}
				}

				float fMagVelocity = sqrtf(p->vx * p->vx + p->vy * p->vy);
				float fMagResponse = sqrtf(fResponseX * fResponseX + fResponseY * fResponseY);

				if (p->px < 0 || p->px > nMapWidth || p->py <0 || p->py > nMapHeight)
					p->bDead = true;

				// � chaque rebond, trouve un angle de collision et influt le rebond en fonction
				if (bCollision)
				{
					p->bStable = true;

					// Calculer le vecteur de r�flexion du vecteur de vitesse des objets
					float dot = p->vx * (fResponseX / fMagResponse) + p->vy * (fResponseY / fMagResponse);

					// Pour prendre en compte le coefficient de frottement appliqu� � chaque rebond
					p->vx = p->fFriction * (-2.0f * dot * (fResponseX / fMagResponse) + p->vx);
					p->vy = p->fFriction * (-2.0f * dot * (fResponseY / fMagResponse) + p->vy);

					//Pour les objets qui disparaissent apr�s un certain nombre de rebons
					if (p->nBounceBeforeDeath > 0)
					{
						p->nBounceBeforeDeath--;
						p->bDead = p->nBounceBeforeDeath == 0;
						if (p->bDead)
						{
							// V�rivie l'action � la mort de l'objet:
							// = 0 Rien ne se passe
							// > 0 L'objet explose
							int nResponse = p->BounceDeathAction();
							if (nResponse > 0)
							{
								Boom(p->px, p->py, nResponse);
								pCameraTrackingObject = nullptr;
							}
						}
					}
				}
				else
				{
					p->px = fPotentialX;
					p->py = fPotentialY;
				}

				// Ne prends pas en compte les mouvement lorsqu'ils sont trop insignifiant 
				if (fMagVelocity < 0.1f) p->bStable = true;
			}

			// Supprime les objets morts de la liste afin qu'ils ne soient plus trait�s. 
			// Comme l'objet est un pointeur unique, il sortira �galement de la port�e, supprimant l'objet automatiquement
			listObjects.remove_if([](unique_ptr<cPhysicsObject>& o) {return o->bDead; });
		}

		// L� on dessine le paysage
		if (!bZoomOut)
		{
			for (int x = 0; x < ScreenWidth(); x++)
				for (int y = 0; y < ScreenHeight(); y++)
				{
					switch (map[(y + (int)fCameraPosY) * nMapWidth + (x + (int)fCameraPosX)])
					{
					case -1:Draw(x, y, PIXEL_SOLID, FG_DARK_BLUE); break;
					case -2:Draw(x, y, PIXEL_QUARTER, FG_BLUE | BG_DARK_BLUE); break;
					case -3:Draw(x, y, PIXEL_HALF, FG_BLUE | BG_DARK_BLUE); break;
					case -4:Draw(x, y, PIXEL_THREEQUARTERS, FG_BLUE | BG_DARK_BLUE); break;
					case -5:Draw(x, y, PIXEL_SOLID, FG_BLUE); break;
					case -6:Draw(x, y, PIXEL_QUARTER, FG_CYAN | BG_BLUE); break;
					case -7:Draw(x, y, PIXEL_HALF, FG_CYAN | BG_BLUE); break;
					case -8:Draw(x, y, PIXEL_THREEQUARTERS, FG_CYAN | BG_BLUE); break;
					case 0:	Draw(x, y, PIXEL_SOLID, FG_CYAN); break;
					case 1:	Draw(x, y, PIXEL_SOLID, FG_DARK_GREEN);	break;
					}
				}

			// Cette boucle permet aux objets de se dessiner
			for (auto& p : listObjects)
			{
				p->Draw(this, fCameraPosX, fCameraPosY);

				cUnit* unit = (cUnit*)pObjectUnderControl;
				if (p.get() == unit)
				{
					// C'est ici que l'on dessine le r�ticule pour viser
					float cx = unit->px + 8.0f * cosf(unit->fShootAngle) - fCameraPosX;
					float cy = unit->py + 8.0f * sinf(unit->fShootAngle) - fCameraPosY;

					Draw(cx, cy, PIXEL_SOLID, FG_BLACK);
					Draw(cx + 1, cy, PIXEL_SOLID, FG_BLACK);
					Draw(cx - 1, cy, PIXEL_SOLID, FG_BLACK);
					Draw(cx, cy + 1, PIXEL_SOLID, FG_BLACK);
					Draw(cx, cy - 1, PIXEL_SOLID, FG_BLACK);

					for (int i = 0; i < 11 * fEnergyLevel; i++)
					{
						Draw(unit->px - 5 + i - fCameraPosX, unit->py - 12 - fCameraPosY, PIXEL_SOLID, FG_GREEN);
						Draw(unit->px - 5 + i - fCameraPosX, unit->py - 11 - fCameraPosY, PIXEL_SOLID, FG_RED);
					}
				}
			}
		}
		else
		{
			for (int x = 0; x < ScreenWidth(); x++)
				for (int y = 0; y < ScreenHeight(); y++)
				{
					float fx = (float)x / (float)ScreenWidth() * (float)nMapWidth;
					float fy = (float)y / (float)ScreenHeight() * (float)nMapHeight;

					switch (map[((int)fy) * nMapWidth + ((int)fx)])
					{
					case -1:Draw(x, y, PIXEL_SOLID, FG_DARK_BLUE); break;
					case -2:Draw(x, y, PIXEL_QUARTER, FG_BLUE | BG_DARK_BLUE); break;
					case -3:Draw(x, y, PIXEL_HALF, FG_BLUE | BG_DARK_BLUE); break;
					case -4:Draw(x, y, PIXEL_THREEQUARTERS, FG_BLUE | BG_DARK_BLUE); break;
					case -5:Draw(x, y, PIXEL_SOLID, FG_BLUE); break;
					case -6:Draw(x, y, PIXEL_QUARTER, FG_CYAN | BG_BLUE); break;
					case -7:Draw(x, y, PIXEL_HALF, FG_CYAN | BG_BLUE); break;
					case -8:Draw(x, y, PIXEL_THREEQUARTERS, FG_CYAN | BG_BLUE); break;
					case 0:	Draw(x, y, PIXEL_SOLID, FG_CYAN); break;
					case 1:	Draw(x, y, PIXEL_SOLID, FG_DARK_GREEN);	break;
					}
				}

			for (auto& p : listObjects)
				p->Draw(this, p->px - (p->px / (float)nMapWidth) * (float)ScreenWidth(),
					p->py - (p->py / (float)nMapHeight) * (float)ScreenHeight(), true);
		}


		// Cette fonction permet de v�rifier la stabilit� du jeu affin d'attendre (dans une autre fonction) que le jeu se stabilise pour continuer � jouer
		bGameIsStable = true;
		for (auto& p : listObjects)
			if (!p->bStable)
			{
				bGameIsStable = false;
				break;
			}

		// Ici on dessine la bare de vie global de chaque �quipe
		for (size_t t = 0; t < vecTeams.size(); t++)
		{
			float fTotalHealth = 0.0f;
			float fMaxHealth = (float)vecTeams[t].nTeamSize;
			for (auto w : vecTeams[t].vecMembers) // Accumule la vie de tout les membres de l'�quipe 
				fTotalHealth += w->fHealth;

			int cols[] = { FG_RED, FG_BLUE, FG_MAGENTA, FG_GREEN };
			Fill(4, 4 + t * 4, (fTotalHealth / fMaxHealth) * (float)(ScreenWidth() - 8) + 4, 4 + t * 4 + 3, PIXEL_SOLID, cols[t]);
		}

		// Affichage du compte � rebours
		if (bShowCountDown)
		{
			wchar_t d[] = L"w$]m.k{\%\x7Fo"; int tx = 4, ty = vecTeams.size() * 4 + 8;
			for (int r = 0; r < 13; r++) {
				for (int c = 0; c < ((fTurnTime < 10.0f) ? 1 : 2); c++) {
					int a = to_wstring(fTurnTime)[c] - 48; if (!(r % 6)) {
						DrawStringAlpha(tx,
							ty, wstring((d[a] & (1 << (r / 2)) ? L" #####  " : L"        ")), FG_BLACK);
						tx += 8;
					}
					else {
						DrawStringAlpha(tx, ty, wstring((d[a] & (1 << (r < 6 ? 1 : 4)) ?
							L"#     " : L"      ")), FG_BLACK); tx += 6; DrawStringAlpha(tx, ty, wstring
							((d[a] & (1 << (r < 6 ? 2 : 5)) ? L"# " : L"  ")), FG_BLACK); tx += 2;
					}
				}ty++; tx = 4;
			}
		}

		// Met � jour les phase de l'IA
		nGameState = nNextState;
		nAIState = nAINextState;

		return true;
	}


	void Boom(float fWorldX, float fWorldY, float fRadius)
	{
		auto CircleBresenham = [&](int xc, int yc, int r)
		{
			int x = 0;
			int y = r;
			int p = 3 - 2 * r;
			if (!r) return;

			auto drawline = [&](int sx, int ex, int ny)
			{
				for (int i = sx; i < ex; i++)
					if (ny >= 0 && ny < nMapHeight && i >= 0 && i < nMapWidth)
						map[ny * nMapWidth + i] = 0;
			};

			while (y >= x) // formule seulement 1/8 de cercle
			{
				//Cercle rempli
				drawline(xc - x, xc + x, yc - y);
				drawline(xc - y, xc + y, yc - x);
				drawline(xc - x, xc + x, yc + y);
				drawline(xc - y, xc + y, yc + x);
				if (p < 0) p += 4 * x++ + 6;
				else p += 4 * (x++ - y--) + 10;
			}
		};

		int bx = (int)fWorldX;
		int by = (int)fWorldY;

		// Forme un crat�re
		CircleBresenham(fWorldX, fWorldY, fRadius);

		// Projette les autres entit�s 
		for (auto& p : listObjects)
		{
			float dx = p->px - fWorldX;
			float dy = p->py - fWorldY;
			float fDist = sqrt(dx * dx + dy * dy);
			if (fDist < 0.0001f) fDist = 0.0001f;
			if (fDist < fRadius)
			{
				p->vx = (dx / fDist) * fRadius;
				p->vy = (dy / fDist) * fRadius;
				p->Damage(((fRadius - fDist) / fRadius) * 0.8f);
				p->bStable = false;
			}
		}

		// Lance les d�bris
		for (int i = 0; i < (int)fRadius; i++)
			listObjects.push_back(unique_ptr<cDebris>(new cDebris(fWorldX, fWorldY)));
	}

	void PerlinNoise1D(int nCount, float* fSeed, int nOctaves, float fBias, float* fOutput)
	{
		for (int x = 0; x < nCount; x++)
		{
			float fNoise = 0.0f;
			float fScaleAcc = 0.0f;
			float fScale = 1.0f;

			for (int o = 0; o < nOctaves; o++)
			{
				int nPitch = nCount >> o;
				int nSample1 = (x / nPitch) * nPitch;
				int nSample2 = (nSample1 + nPitch) % nCount;
				float fBlend = (float)(x - nSample1) / (float)nPitch;
				float fSample = (1.0f - fBlend) * fSeed[nSample1] + fBlend * fSeed[nSample2];
				fScaleAcc += fScale;
				fNoise += fSample * fScale;
				fScale = fScale / fBias;
			}

			fOutput[x] = fNoise / fScaleAcc;
		}
	}

	void CreateMap()
	{
		float* fSurface = new float[nMapWidth];
		float* fNoiseSeed = new float[nMapWidth];
		for (int i = 0; i < nMapWidth; i++)
			fNoiseSeed[i] = (float)rand() / (float)RAND_MAX;

		fNoiseSeed[0] = 0.5f;
		PerlinNoise1D(nMapWidth, fNoiseSeed, 8, 2.0f, fSurface);

		for (int x = 0; x < nMapWidth; x++)
			for (int y = 0; y < nMapHeight; y++)
			{
				if (y >= fSurface[x] * nMapHeight)
					map[y * nMapWidth + x] = 1;
				else
				{
					if ((float)y < (float)nMapHeight / 3.0f)
						map[y * nMapWidth + x] = (-8.0f * ((float)y / (nMapHeight / 3.0f))) - 1.0f;
					else
						map[y * nMapWidth + x] = 0;
				}
			}

		delete[] fSurface;
		delete[] fNoiseSeed;
	}


};



int main()
{
	Game game;
	game.ConstructConsole(256, 160, 6, 6);
	game.Start();
	return 0;
}

