
# Projet de C++ B3 du 04/2021

## De:

### -Costin Gibaux
### -Rafaël Metayer
### -Axel Storme 

# Description du projet:

### Jeu de bataille par équipe sur une colline en deux démensions.
### Le projet ayant pour objectif de développer un jeu en C++ avec une certaine complexité. 

# Fonctionnement:

### Lorsque le jeu se lance, la colline se génère aléatoirement et les 4 équipes apparaissent, chaque équipe étant composée de 4 personnages.
### Une fois les équipes apparues la première équipe à jouer est celle du joueur (les 3 autres sont contrôlées par une IA)
## Le tour de chaque équipe se divise en plusieurs parties:

### - Déplacer son personnage grace au curseur. Le curseur va vers la droite (avec "S") ou vers la gauche (avec "A"). Puis le personnage saute (avec "Z") dans la direction du curseur.
### - Une fois en position, grace au curseur, on peut charger un tir avec la touche espace, plus on la maintient, plus la barre de chargement augmente, et plus le missile est projeté loin.
### - Puis la camera va suivre le missile jusqu'à ce qu'il explose, une fois qu'il explose le missile fait des dégâts aux personnages touchés, détruit du terrain et crée des débrits. Une fois les débrits disparus (après 5 rebonds) la caméra se recentre sur un personnage de l'équipe suivante qui joue à son tour.

## Comme dit précédement les autres équipes sont contrôlées par une IA. Elle a 3 manières de jouer son tour:

### - Stratégie agressive : S'avance vers le centre de la carte pour attaquer une unité.
### - Stratégie défensive : S'éloigner de son équipe afin d'éviter d'être touché à plusieurs et tirer de plus loin.
### - Stratégie passive   : Rester à sa position initiale et tirer.

### Parmis les stratégies proposées, si l'IA ne trouve pas de solution optimale, alors elle va tenter de se frayer un chemin en tirant sur le terrain. Ce faisant elle s'expose au risque de s'infliger des dégâts.

## La partie se termine quand 3 des 4 équipes ont été éliminées, c'est à dire que tous leurs personnages ont perdue tout leurs points de vie.
### En récompense, l'équipe victorieuse se voit gratifiée d'une pluie de missile sur toute la carte :) (ce qui fait grandement chuter vos FPS)

